/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef INTERFACE_PIPE_TO_ROS
#define INTERFACE_PIPE_TO_ROS

#include "common_defs.h"
#include "input_interface_named_pipe.h"
#include "output_interface_ros.h"

// Forward decl
struct camera_image_metadata_t;

//------------------------------------------------------------------------------------------------------------------------------
// Pipe to Ros class that converts named-pipe messages to ros messages
//------------------------------------------------------------------------------------------------------------------------------
class CameraPipeToRos
{
public:
    static CameraPipeToRos* Create(int argc, char **argv);
    void Run();
    void Destroy();

    // Callback called by the input interface when it receives the image data
    void PipeImageData(int id, camera_image_metadata_t* pImageMetadata, uint8_t* pImagePixels);

private:
    // Prevent direct instantiations and instead call Create and Destroy
    CameraPipeToRos()  { }
    ~CameraPipeToRos() { }
    void Cleanup();

    Status Initialize(int argc, char **argv);

    CameraNamedPipe* m_pInputPipeInterface[MaxNamedPipes];  ///< Input named pipe interfaces
    CameraRosOutput* m_pOutputRosInterface[MaxNamedPipes];  ///< Output ros interfaces
    ros::NodeHandle* m_pRosNodeHandle;                      ///< ROS handle
};

#endif // end #define INTERFACE_PIPE_TO_ROS

// /*******************************************************************************
//  * Copyright 2020 ModalAI Inc.
//  *
//  * Redistribution and use in source and binary forms, with or without
//  * modification, are permitted provided that the following conditions are met:
//  *
//  * 1. Redistributions of source code must retain the above copyright notice,
//  *    this list of conditions and the following disclaimer.
//  *
//  * 2. Redistributions in binary form must reproduce the above copyright notice,
//  *    this list of conditions and the following disclaimer in the documentation
//  *    and/or other materials provided with the distribution.
//  *
//  * 3. Neither the name of the copyright holder nor the names of its contributors
//  *    may be used to endorse or promote products derived from this software
//  *    without specific prior written permission.
//  *
//  * 4. The Software is used solely in conjunction with devices provided by
//  *    ModalAI Inc.
//  *
//  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//  * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//  * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//  * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
//  * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
//  * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
//  * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
//  * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
//  * POSSIBILITY OF SUCH DAMAGE.
//  ******************************************************************************/

#include <ros/ros.h>
#include <std_msgs/Header.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Bool.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <modal_pipe_client.h>
#include <modal_pipe_interfaces.h>

#include "debug_log.h"
#include "output_interface_ros.h"
#include "interface_pipe_to_ros.h"

// Global variables to this file only
static CameraPipeToRos* g_pCameraPipeToRos = NULL;

// Global function prototypes
void PipeImageDataCb(int id, camera_image_metadata_t* pImageMetadata, uint8_t* pImagePixels);

//------------------------------------------------------------------------------------------------------------------------------
// Callback to get the image data
//------------------------------------------------------------------------------------------------------------------------------
void PipeImageDataCb(int id, camera_image_metadata_t* pImageMetadata, uint8_t* pImagePixels)
{
    g_pCameraPipeToRos->PipeImageData(id, pImageMetadata, pImagePixels);
}

//------------------------------------------------------------------------------------------------------------------------------
// Perform any necessary clean up actions before the object gets destroyed
//------------------------------------------------------------------------------------------------------------------------------
void CameraPipeToRos::Cleanup()
{
    for (int i = 0; i < MaxNamedPipes; i++)
    {
        if (m_pInputPipeInterface[i] != NULL)
        {
            m_pInputPipeInterface[i]->Destroy();
            m_pInputPipeInterface[i] = NULL;
        }

        if (m_pOutputRosInterface[i] != NULL)
        {
            m_pOutputRosInterface[i]->Destroy();
            m_pOutputRosInterface[i] = NULL;
        }
    }
}

//------------------------------------------------------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------------------------------------------------------
void CameraPipeToRos::Destroy()
{
    Cleanup();
    delete this;
}

//------------------------------------------------------------------------------------------------------------------------------
// Process the image data
//------------------------------------------------------------------------------------------------------------------------------
void CameraPipeToRos::PipeImageData(int id, camera_image_metadata_t* pImageMetadata, uint8_t* pImagePixels)
{
    m_pOutputRosInterface[id]->ProcessImageData(pImageMetadata, pImagePixels);
}

//------------------------------------------------------------------------------------------------------------------------------
// Anything to do prior to running
//------------------------------------------------------------------------------------------------------------------------------
void CameraPipeToRos::Run()
{

}

//------------------------------------------------------------------------------------------------------------------------------
// Create an instance of the class, initialize and return the object instance. If there are any problems during initialization
// will be result in the object not being instantiated
//------------------------------------------------------------------------------------------------------------------------------
CameraPipeToRos* CameraPipeToRos::Create(int argc, char **argv)
{
    g_pCameraPipeToRos = new CameraPipeToRos;

    if (g_pCameraPipeToRos != NULL)
    {
        if (g_pCameraPipeToRos->Initialize(argc, argv) != S_OK)
        {
            LOG_FATAL("\n------voxl-mpa-cam-ros: Failed to initialize");
            g_pCameraPipeToRos->Destroy();
            g_pCameraPipeToRos = NULL;
        }
    }

    return g_pCameraPipeToRos;
}

//------------------------------------------------------------------------------------------------------------------------------
// Initialize the object instance. Return S_ERROR on any errors.
//------------------------------------------------------------------------------------------------------------------------------
Status CameraPipeToRos::Initialize(int argc, char **argv)
{
    Status status = S_OK;

    ros::init(argc, argv, "voxl_ros_to_pipe");

    m_pRosNodeHandle = new ros::NodeHandle("~");

    if (m_pRosNodeHandle != NULL)
    {
        char rosMsgTopicStr[MAX_NAME_LENGTH];
        char rosPipeNameStr[MAX_NAME_LENGTH];
        std::string rosTopic;
        std::string pipeName;

        // Read all entries with argument name "ros_msg_type_0", "ros_msg_type_1", "ros_msg_type_2" and so on till you find an
        // entry which is not present. "Not present" entry indicates the previous entry was the last one and we break out of the
        // loop
        for (int id = 0; ; id++)
        {
            sprintf(&rosMsgTopicStr[0], "ros_topic_%d", id);
            sprintf(&rosPipeNameStr[0], "pipe_name_%d", id);

            // The default value of "notpresent" indicates there are no more entries and we break out of the loop
            m_pRosNodeHandle->param<std::string>(&rosMsgTopicStr[0], rosTopic,   "notpresent");
            m_pRosNodeHandle->param<std::string>(&rosPipeNameStr[0], pipeName,   "notpresent");

            // The default value of "notpresent" indicates there are no more entries and we break out of the loop
            if (pipeName.find("notpresent") != std::string::npos)
            {
                break;
            }
            else
            {
                if (pipeName.find("disabled") == std::string::npos)
                {
                    if (rosTopic.find("notpresent") != std::string::npos)
                    {
                        LOG_FATAL("\n------voxl-ros-to-pipe: FATAL: ros-topic not given for pipe %s!", pipeName.c_str());
                        status = S_ERROR;
                    }
                }
                else
                {
                    continue;
                }
            }

            if (status == S_ERROR)
            {
                // Continue and print any more error messages and then bail out
                continue;
            }

            m_pOutputRosInterface[id] = CameraRosOutput::Create();

            if (m_pOutputRosInterface[id] != NULL)
            {
                OutputRosInitData initData = { 0 };

                initData.pRosNodeHandle = m_pRosNodeHandle;
                initData.pRosTopic      = rosTopic.c_str();

                status = m_pOutputRosInterface[id]->Initialize(&initData);

                InputInterfaceData inputData    = { 0 };
                inputData.ImageReceivedCallback = PipeImageDataCb;
                inputData.pipeName              = pipeName.c_str();
                inputData.id                    = id;

                m_pInputPipeInterface[id] = CameraNamedPipe::Create();

                if (m_pInputPipeInterface[id] != NULL)
                {
                    status = m_pInputPipeInterface[id]->Initialize(&inputData);
                }
                else
                {
                    status = S_ERROR;
                }
            }
            else
            {
                status = S_ERROR;
            }
        }
    }
    else
    {
        LOG_FATAL("\n------voxl-ros-to-pipe: FATAL: Cannot create ros handle!");
        status = S_ERROR;
    }

    return status;
}
